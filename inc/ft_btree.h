/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 10:56:04 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 10:59:11 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_BTREE_H
# define FT_BTREE_H

# include <stdlib.h>

# include "ft_mem.h"

typedef struct s_btree		t_btree;

struct			s_btree
{
	void					*content;
	t_btree					*left;
	t_btree					*right;
};

t_btree			*btree_create(void *content);
t_btree			*btree_insert(t_btree **root, void *content, int (*cmp)());
void			btree_inorder(t_btree *root, void (*act)());
void			node_if(t_btree *root, void (*act)(), int (*assert)());
void			node_data(t_btree *root, void (*act)(), void *data);
void			node_if_data(t_btree *root, void (*act)(), int (*assert)(),
								void *data);
void			btree_postorder(t_btree *root, void (*act)());
void			btree_preorder(t_btree *root, void (*act)());
void			btree_clean(t_btree *root, void (*clr)());
void			node_rev(t_btree *root, void (*act)());
void			node_rev_if(t_btree *root, void (*act)(), int (*assert)());
void			node_rev_data(t_btree *root, void (*act)(), void *data);
void			node_rev_if_data(t_btree *root, void (*act)(), int (*assert)(),
									void *data);
t_btree			*btree_search(t_btree *root, void *content, int (*assert)());

#endif
