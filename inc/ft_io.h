/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_io.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 10:55:02 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 10:55:04 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_IO_H
# define FT_IO_H

# include <unistd.h>

void		ft_red(char *s);
void		ft_blue(char *s);
void		ft_cyan(char *s);
void		ft_green(char *s);
void		ft_yellow(char *s);
void		ft_magenta(char *s);
void		ft_putchar(char c);
void		ft_putchar_fd(char c, int fd);
void		ft_putstr_fd(char const *s, int fd);
void		ft_putendl_fd(char const *s, int fd);
void		ft_putstr(char const *s);
void		ft_putendl(char const *s);
void		ft_putnbr_fd(int n, int fd);
void		ft_putnbr(int nb);

#endif
