/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 10:56:20 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:01:52 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STR_H
# define FT_STR_H

# include <string.h>

# include "ft_mem.h"

size_t		ft_strlen(const char *s);
size_t		ft_strclen(char *s, char c);

char		*ft_strnew(size_t size);
char		*ft_strcpy(char *dst, const char *src);
char		*ft_strncpy(char *dst, const char *src, size_t n);
void		ft_strclr(char *s);
void		ft_strdel(char **as);
char		*ft_strdup(const char *s1);
char		*ft_strcat(char *s1, const char *s2);
char		*ft_strncat(char *s1, const char *s2, size_t n);
char		*ft_strjoin(char const *s1, char const *s2);
char		*ft_strsub(char const *s, unsigned int start, size_t n);

int			ft_atoi(const char *s);
char		*ft_itoa(int n);
int			ft_strindex(const char *s, const char c);
int			ft_toupper(int c);
int			ft_tolower(int c);
char		*ft_strtolower(char *s);
char		*ft_strtoupper(char *s);
char		*ft_strrev(char *str);
char		*ft_strtrim(char const *s);
char		**ft_strsplit(char const *s, char c);
void		ft_striter(char *s, void (*f)(char *));
char		*ft_strmap(char const *s, char (*f)(char));
char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));

#endif
