/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 10:55:56 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:02:22 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_UTILS_H
# define FT_UTILS_H

# include <string.h>
# include <limits.h>

# include "ft_str.h"

# define MAX_SQRT 46341
# define MAX_POWER 2147483647

size_t		ft_nbrlen(int n);
size_t		ft_ll_nbrlen(long long n);
size_t		ft_base_nbrlen(unsigned long long n, int base);
void		ft_swap(int *a, int *b);
void		ft_sort_integer_table(int *tab, int size);

int			ft_sqrt(int nb);
int			ft_next_sqrt(int nb);
int			ft_last_sqrt(int nb);
int			ft_next_bit(int nb);
int			ft_last_bit(int nb);
int			ft_next_power(int nb, int power);
int			ft_last_power(int nb, int power);
#endif
