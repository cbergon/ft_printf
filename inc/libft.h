/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 10:56:29 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 10:56:31 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include "assert.h"
# include "ft_io.h"
# include "ft_lst.h"
# include "ft_mem.h"
# include "ft_str.h"
# include "get_next_line.h"
# include "utils.h"
# include "ft_btree.h"
# include "ft_buf.h"
# include "ft_printf.h"

typedef struct		s_coord
{
	double			x;
	double			y;
}					t_coord;

#endif
