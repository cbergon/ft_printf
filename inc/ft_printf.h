/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:16:39 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:16:44 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <inttypes.h>
# include <stdio.h>
# include <unistd.h>

# include "ft_buf.h"
# include "assert.h"

# define ERR -1
# define FAILURE 0
# define SUCCESS 1
# define LUP 1
# define LLOW 2
# define LL 4
# define H 8
# define HH 16
# define CONVERSION "cspdiouxXf%m"
# define FLAG "+- 0#"

/*
**	(1 << i)
*/
# define FLAG_PLUS 1
# define FLAG_MINUS 2
# define FLAG_SPACE 4
# define FLAG_ZERO 8
# define FLAG_HASH 16

# define FUNCS 12

typedef struct	s_conv
{
	int		flag;
	int		width;
	int		prec;
	int		length;
	int		type;
}				t_conv;

# define BASE_OCTAL "01234567"
# define BASE_DECIMAL "0123456789"
# define BASE_LOWER "0123456789abcdef"
# define BASE_UPPER "0123456789ABCDEF"
# define DOUB_BUFSIZ 256

typedef unsigned char		t_uc;
typedef unsigned long long	t_ull;
typedef long long			t_ll;

/*
** s_ddim is a print dimension helper for the floating point conversion of
** printf
*/
typedef struct	s_ddim
{
	int			elem;
	int			total;
	char		sign;
	int			prec;
	int			zero_pad;
	int			left_pad;
	int			right_pad;
	t_ull		real;
}				t_ddim;

typedef int		(*t_handler)(t_buf *buf, va_list ap, t_conv *conv);

typedef struct	s_iull_handler
{
	int			(*handle_format)(t_conv *conv);
	int			(*handle_len)(t_ull ull, t_conv *conv);
	int			(*push_to_buf)(t_buf *buf, t_conv * conv, t_ull ull, int len);
}				t_iull_handler;

/*
** FT_PRINT.C
*/
int				ft_printf(const char *format, ...);
int				ft_dprintf(int fd, const char *format, ...);
int				ft_vprintf(const char *format, va_list ap);
int				ft_vdprintf(int fd, const char *format, va_list ap);

/*
** GET_T_CONV.c
*/
int				get_flag(const char *format, t_conv *conv, int *index);
int				get_width(const char *format, t_conv *conv, int *index);
int				get_prec(const char *format, t_conv *conv, int *index);
int				get_length(const char *format, t_conv *conv, int *index);
int				get_type(const char *format, t_conv *conv, int *index);

/*
** PROCESS.C
*/
int				process(const char *format, t_buf *buf, va_list ap);

/*
** CONVERT_STRINGS.C
*/
int				char_conv(t_buf *buf, va_list ap, t_conv *conv);
int				pct_conv(t_buf *buf, va_list ap, t_conv *conv);
int				str_conv(t_buf *buf, va_list ap, t_conv *conv);

/*
** CONVERT_SIGNED.C
*/
int				d_conv(t_buf *buf, va_list ap, t_conv *conv);

/*
** CONVERT_UNSIGNED.C
*/
int				unsigned_conv(t_buf *buf, va_list ap, t_conv *conv);

/*
** CONVERT_DOUBLE.C
*/
double			rounder(int p);
size_t			get_ull_len(uintmax_t ull, size_t base);
int				double_conv(t_buf *buf, va_list ap, t_conv *conv);
void			fpf_ultobuf(t_ull ull, size_t base, char *charset, char *buf);

/*
** I_UNSIGNED.C
*/
t_iull_handler	assign_unsigned(void);

/*
** I_OCTAL.C
*/
t_iull_handler	assign_octal(void);

/*
** I_HEXA.C
*/
t_iull_handler	assign_hexa(void);

/*
** I_POINTER.C
*/
t_iull_handler	assign_pointer(void);

/*
** UTILS.C
*/
int				pad_before(t_buf *buf, t_conv *conv, int len);
int				pad_after(t_buf *buf, t_conv *conv, int len);
int				err_handler(t_buf *buf, va_list ap, t_conv *conv);

/*
** DDIM.C
*/
t_ddim			*get_dims(long double *d, t_conv *conv);

/*
** DDIM_TO_BUF.C
*/
int				ddim_to_buf_sign(t_ddim *ddim, t_buf *buf);
int				ddim_to_buf(t_buf *buf, char c, int n);

#endif
