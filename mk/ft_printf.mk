HEADER += inc/ft_printf.h

TMP_SRC := \
	ft_printf.c\
	process.c\
	init_conv.c\
	convert_strings.c\
	convert_signed.c\
	integer_utils.c\
	convert_unsigned.c\
	i_hexa.c\
	i_unsigned.c\
	i_octal.c\
	i_pointer.c\
	convert_double.c\
	ddim.c\
	ddim_to_buf.c\
	utils.c\

TMP_DIR := ft_printf/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))
