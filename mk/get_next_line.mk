HEADER += inc/get_next_line.h

TMP_SRC := get_next_line.c
TMP_DIR := get_next_line/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))