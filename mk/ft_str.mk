HEADER += inc/ft_str.h

TMP_SRC := \
	ft_atoi.c\
	ft_itoa.c\
	ft_itoa_base.c\
	ft_strcat.c\
	ft_strclr.c\
	ft_strcpy.c\
	ft_strdel.c\
	ft_strdup.c\
	ft_strlen.c\
	ft_strmap.c\
	ft_strnew.c\
	ft_strrev.c\
	ft_strsub.c\
	ft_strclen.c\
	ft_striter.c\
	ft_strjoin.c\
	ft_strlcat.c\
	ft_strmapi.c\
	ft_strncat.c\
	ft_strncpy.c\
	ft_strtrim.c\
	ft_tolower.c\
	ft_toupper.c\
	ft_striteri.c\
	ft_strsplit.c\
	ft_strtolower.c\
	ft_strtoupper.c\
	ft_strindex.c\


TMP_DIR := ft_str/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))