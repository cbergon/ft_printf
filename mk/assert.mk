HEADER += inc/assert.h

TMP_SRC := \
	ft_strstr.c\
	ft_strchr.c\
	ft_strcmp.c\
	ft_strequ.c\
	ft_isalnum.c\
	ft_isalpha.c\
	ft_isascii.c\
	ft_isdigit.c\
	ft_islower.c\
	ft_isupper.c\
	ft_strncmp.c\
	ft_strnequ.c\
	ft_strnstr.c\
	ft_strrchr.c\
	ft_isprint.c\
	ft_isnumber.c\
	ft_isnegative.c\

TMP_DIR := assert/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))
