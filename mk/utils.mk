HEADER += inc/utils.h

TMP_SRC := \
	ft_bit.c\
	ft_sqrt.c\
	ft_swap.c\
	ft_nbrlen.c\
	ft_power.c\
	ft_sort_integer_table.c\

TMP_DIR := utils/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))