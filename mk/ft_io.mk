HEADER += inc/ft_io.h

TMP_SRC := \
	ft_red.c\
	ft_blue.c\
	ft_cyan.c\
	ft_green.c\
	ft_putnbr.c\
	ft_putstr.c\
	ft_yellow.c\
	ft_magenta.c\
	ft_putchar.c\
	ft_putendl.c\
	ft_putstr_fd.c\
	ft_putnbr_fd.c\
	ft_putchar_fd.c\
	ft_putendl_fd.c\

TMP_DIR := ft_io/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))