HEADER += inc/ft_lst.h

TMP_SRC := \
	ft_lstadd.c\
	ft_lstdel.c\
	ft_lstlen.c\
	ft_lstmap.c\
	ft_lstnew.c\
	ft_lstiter.c\
	ft_lstdelone.c\

TMP_DIR := ft_lst/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))
