HEADER += inc/ft_mem.h

TMP_SRC := \
	ft_bzero.c\
	ft_memchr.c\
	ft_memcmp.c\
	ft_memcpy.c\
	ft_memdel.c\
	ft_memset.c\
	ft_memccpy.c\
	ft_memmove.c\
	ft_memalloc.c\


TMP_DIR := ft_mem/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))