HEADER += inc/ft_btree.h

TMP_SRC := \
	ft_btree_clean.c\
	ft_btree_create.c\
	ft_btree_insert.c\
	ft_btree_search.c\
	ft_btree_ordered.c\
	ft_btree_inorder.c\
	ft_btree_rev_inorder.c\

TMP_DIR := ft_btree/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))
