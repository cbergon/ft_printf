HEADER += inc/ft_buf.h

TMP_SRC := \
	buf_new.c\
	buf_push.c\
	buf_grow.c\
	buf_free.c\

TMP_DIR := ft_buf/

DEP_DIR := $(DEP_DIR) $(TMP_DIR)
SRC := $(SRC) $(addprefix $(TMP_DIR), $(TMP_SRC))