#
#
#			LIBFT
#
#

NAME := libftprintf.a
HEADER := inc/libft.h
INCLUDES = inc/
SRC_DIR := src/
OBJ_DIR = obj/

CC := gcc
CFLAGS := -Wall -Wextra -Werror


RED="\\033[31m"
GRE="\\033[32m"
YEL="\\033[33m"
BLU="\\033[34m"
MAG="\\033[35m"
CYA="\\033[36m"
NC="\\033[0m"
CHE="\\xE2\\x9C\\x94"
OK="$(GRE)$(CHE)$(NC)"
SUCCESS = "$(GRE)Done$(NC)"

DEBUG := no
ifeq ($(DEBUG), g3)
	CFLAGS += -g3
else ifeq ($(DEBUG), sanitize)
	CFLAGS += -fsanitize=address -g3
endif

include mk/assert.mk
include mk/ft_io.mk
include mk/ft_lst.mk
include mk/ft_mem.mk
include mk/ft_str.mk
include mk/get_next_line.mk
include mk/utils.mk
include mk/ft_btree.mk
include mk/ft_buf.mk
include mk/ft_printf.mk

OBJ := $(patsubst %.c, $(OBJ_DIR)%.o, $(SRC))
SRC := $(addprefix $(SRC_DIR), $(SRC))
RECIPE_INC := $(foreach iter,$(INCLUDES), -iquote $(iter))
RECIPE_DEP := $(foreach iter,$(DEP_DIR), $(OBJ_DIR)$(iter))
RECIPE_SUCCESS := $(if $(strip $(QUIET)),\t$(OK),$(SUCCESS))

QUIET := @
RECIPE_BQUIET := $(if $(strip $(QUIET)),"\\rCompiling\\t")
RECIPE_EQUIET := $(if $(strip $(QUIET)),"\\c")


all : $(NAME)

$(NAME) :  $(OBJ_DIR) $(OBJ)
	@echo "\\nCreating exec\t$(CYA)$(NAME)$(NC)$(RECIPE_EQUIET)"
	$(QUIET)ar rc $(NAME) $(OBJ)
	$(QUIET)ranlib	$@
	@echo "\\n$(RECIPE_SUCCESS)"
	
$(OBJ_DIR) :
	$(QUIET)mkdir -p $@ $(RECIPE_DEP)

$(OBJ_DIR)%.o : $(SRC_DIR)%.c | $(OBJ_DIR)
	@echo "\r$(RECIPE_BQUIET)$(BLU)$<$(NC)$(RECIPE_EQUIET)"
	$(QUIET)$(CC) $(CFLAGS) -o $@ -c $< $(RECIPE_INC)

clean :
	@echo "Deleting all \t$(RED)objects$(NC)$(RECIPE_QUIET)"
	$(QUIET)rm -rf $(OBJ_DIR)
	@echo "$(RECIPE_SUCCESS)"

cleanf :
	@echo "Deleting exec \t$(CYA)$(NAME)$(NC)$(RECIPE_QUIET)"
	$(QUIET)rm -f $(NAME)
	@echo "$(RECIPE_SUCCESS)"

fclean : clean cleanf

proper : all clean

re : fclean all

.PHONY: proper re

bin : all
	gcc main.c $(CFLAGS) $(RECIPE_INCLUDE) $(NAME) $(LIBFT)

test:
	make && make bin && ./a.out
