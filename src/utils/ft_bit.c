/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:31:45 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:31:48 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int	ft_bit(int nb, int i)
{
	if (nb < i)
		return (i);
	return (ft_bit(nb, i << 1));
}

int			ft_next_bit(int nb)
{
	return (ft_bit(nb, 1));
}

int			ft_last_bit(int nb)
{
	return (ft_bit(nb, 1) >> 1);
}
