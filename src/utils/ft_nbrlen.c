/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:24:02 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:24:21 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

size_t	ft_nbrlen(int n)
{
	size_t		size;

	size = 0;
	if (n == 0)
		return (1);
	if (n == INT_MIN)
		return (10);
	while (n != 0)
	{
		n /= 10;
		size++;
	}
	return (size);
}

size_t	ft_ll_nbrlen(long long n)
{
	size_t		size;

	size = 0;
	if (n == 0)
		return (1);
	if (n == LLONG_MIN)
		return (19);
	while (n != 0)
	{
		n /= 10;
		size++;
	}
	return (size);
}

size_t	ft_base_nbrlen(unsigned long long n, int base)
{
	size_t		size;

	size = 0;
	if (n == 0)
		return (1);
	while (n != 0)
	{
		n /= base;
		size++;
	}
	return (size);
}
