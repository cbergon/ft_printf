/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:31:45 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:31:48 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

static int	ft_power(int nb, int power, int i)
{
	if (nb < i)
		return (i);
	else
		return (ft_power(nb, power, i * power));
}

int			ft_next_power(int nb, int power)
{
	return (ft_power(nb, power, 1));
}

int			ft_last_power(int nb, int power)
{
	return (ft_power(nb, power, 1) / power);
}
