/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <clbergon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/06 18:06:06 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:24:41 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_sqrt(int nb)
{
	int		i;

	i = 1;
	while (i < MAX_SQRT)
	{
		if (nb == i * i)
			return (i);
		else
			i++;
	}
	return (0);
}

int		ft_next_sqrt(int nb)
{
	int		i;

	i = 1;
	while (i < MAX_SQRT)
	{
		if (nb == i * i)
			return (i);
		else
			i++;
	}
	return (ft_next_sqrt(nb + 1));
}

int		ft_last_sqrt(int nb)
{
	int		i;

	i = 1;
	while (i < MAX_SQRT)
	{
		if (nb == i * i)
			return (i);
		else
			i++;
	}
	return (ft_last_sqrt(nb - 1));
}
