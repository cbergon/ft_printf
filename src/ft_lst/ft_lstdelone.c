/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <clbergon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 20:44:23 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/29 20:47:02 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lst.h"

void	ft_lstdelone(t_list **alst, void (*del)(void *, size_t))
{
	t_list *head;

	if (alst == 0)
		return ;
	if (*alst == 0)
		return ;
	head = *alst;
	(*del)(head->content, head->content_size);
	free(head);
	*alst = 0;
}
