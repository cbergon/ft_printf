/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <clbergon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 20:43:51 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/29 20:44:00 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_lst.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list *head;
	t_list *current;

	if (!lst)
		return (0);
	if (!(*f))
		return (0);
	head = (*f)(lst);
	current = head;
	lst = lst->next;
	while (lst)
	{
		current->next = (*f)(lst);
		if (!current->next)
			return (0);
		current = current->next;
		lst = lst->next;
	}
	return (head);
}
