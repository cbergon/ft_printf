/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 13:38:39 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/20 13:39:26 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "assert.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*str;
	int		size;

	str = (char *)s;
	size = ft_strlen(s);
	while (size != 0 && str[size] != (unsigned char)c)
		size--;
	if (str[size] == (unsigned char)c)
		return (str + size);
	return (0);
}
