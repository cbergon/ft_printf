/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 13:39:40 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/22 15:50:01 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "assert.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t n)
{
	int	i;
	int	j;
	int	size;
	int	size_needle;

	i = 0;
	j = 0;
	size = (int)n;
	size_needle = ft_strlen(needle);
	if (size_needle == 0)
		return ((char*)haystack);
	while (haystack[i] != '\0' && size-- >= size_needle)
	{
		while (needle[j] == haystack[i + j])
		{
			if (j == size_needle - 1)
				return ((char*)haystack + i);
			j++;
		}
		j = 0;
		i++;
	}
	return (NULL);
}
