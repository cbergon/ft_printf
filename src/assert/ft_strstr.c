/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/09 16:10:02 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/09 16:24:11 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "assert.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	int		i;
	int		j;
	int		size_needle;

	i = 0;
	j = 0;
	size_needle = ft_strlen(needle);
	if (size_needle == 0)
		return ((char*)haystack);
	while (haystack[i] != '\0')
	{
		while (needle[j] == haystack[i + j])
		{
			if (j == size_needle - 1)
				return ((char*)haystack + i);
			j++;
		}
		j = 0;
		i++;
	}
	return (0);
}
