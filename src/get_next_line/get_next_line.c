/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:25:35 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:25:40 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	split_lines(t_fd_list *current, char **line)
{
	size_t				size_line;
	size_t				size;
	char				*tmp;

	size_line = ft_strclen(current->content, '\n');
	if (!(*line = ft_strsub(current->content, 0, size_line)))
		return (-1);
	size = ft_strlen(current->content);
	tmp = ft_strsub(current->content, size_line + 1, size - size_line - 1);
	if (!tmp)
		return (-1);
	free(current->content);
	current->content = tmp;
	return (1);
}

static int	join_buff(t_fd_list *current, char *buff)
{
	char				*tmp;

	if (!(tmp = ft_strjoin(current->content, buff)))
		return (0);
	free(current->content);
	current->content = tmp;
	return (1);
}

static int	read_more(int fd, t_fd_list *current, char **line)
{
	int					ret;
	char				buff[BUFF_SIZE + 1];

	while ((ret = read(fd, buff, BUFF_SIZE)) > 0)
	{
		buff[ret] = '\0';
		if (!(join_buff(current, buff)))
			return (-1);
		if (ft_strchr(current->content, '\n'))
			return (split_lines(current, line));
	}
	if (ret == 0 && current->content[0])
	{
		if (!(join_buff(current, "\n")))
			return (-1);
		return (split_lines(current, line));
	}
	return (ret);
}

int			get_next_line(const int fd, char **line)
{
	static t_fd_list	*fd_list;
	t_fd_list			*current;

	current = fd_list;
	while (current && current->fd != fd)
		current = current->next;
	if (!current)
	{
		if (!(current = (t_fd_list *)malloc(sizeof(t_fd_list))))
			return (-1);
		current->fd = fd;
		current->content = ft_strnew(0);
		current->next = fd_list;
		fd_list = current;
	}
	else if (ft_strchr(current->content, '\n'))
		return (split_lines(current, line));
	return (read_more(fd, current, line));
}
