/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_rev_inorder.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:21:59 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:23:25 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	node_rev(t_btree *root, void (*act)())
{
	if (root == NULL)
		return ;
	node_rev(root->right, act);
	act(root->content);
	node_rev(root->left, act);
}

void	node_rev_if(t_btree *root, void (*act)(), int (*assert)())
{
	if (root == NULL)
		return ;
	node_rev_if(root->right, act, assert);
	if (assert(root))
		act(root->content);
	node_rev_if(root->left, act, assert);
}

void	node_rev_data(t_btree *root, void (*act)(), void *data)
{
	if (root == NULL)
		return ;
	node_rev_data(root->right, act, data);
	act(root->content, data);
	node_rev_data(root->left, act, data);
}

void	node_rev_if_data(t_btree *root, void (*act)(), int (*assert)(),
							void *data)
{
	if (root == NULL)
		return ;
	node_rev_if_data(root->right, act, assert, data);
	if (assert(root))
		act(root->content, data);
	node_rev_if_data(root->left, act, assert, data);
}
