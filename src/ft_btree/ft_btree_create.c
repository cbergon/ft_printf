/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_create.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:21:18 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:21:19 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

t_btree		*btree_create(void *content)
{
	t_btree		*ret;

	if ((ret = (t_btree *)ft_memalloc(sizeof(t_btree))) == NULL)
		return (NULL);
	ret->content = content;
	return (ret);
}
