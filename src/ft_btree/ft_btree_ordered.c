/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_ordered.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:21:47 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:21:50 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_preorder(t_btree *root, void (*act)())
{
	if (root == NULL)
		return ;
	act(root);
	btree_preorder(root->left, act);
	btree_preorder(root->right, act);
}

void	btree_postorder(t_btree *root, void (*act)())
{
	if (root == NULL)
		return ;
	btree_postorder(root->left, act);
	btree_postorder(root->right, act);
	act(root);
}
