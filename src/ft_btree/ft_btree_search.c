/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_search.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:22:08 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:22:09 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

t_btree			*btree_search(t_btree *node, void *content, int (*cmp)())
{
	void		*lhs;
	void		*rhs;

	if (node == NULL)
		return (NULL);
	if (cmp(node->content, content) == 0)
		return (node);
	lhs = btree_search(node->left, content, cmp);
	rhs = btree_search(node->right, content, cmp);
	if (lhs != NULL)
		return (lhs);
	return (rhs);
}
