/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_inorder.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:21:29 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:21:31 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_inorder(t_btree *root, void (*act)())
{
	if (root == NULL)
		return ;
	btree_inorder(root->left, act);
	act(root->content);
	btree_inorder(root->right, act);
}

void	node_if(t_btree *root, void (*act)(), int (*assert)())
{
	if (root == NULL)
		return ;
	node_if(root->left, act, assert);
	if (assert(root))
		act(root->content);
	node_if(root->right, act, assert);
}

void	node_data(t_btree *root, void (*act)(), void *data)
{
	if (root == NULL)
		return ;
	node_data(root->left, act, data);
	act(root->content, data);
	node_data(root->right, act, data);
}

void	node_if_data(t_btree *root, void (*act)(), int (*assert)(), void *data)
{
	if (root == NULL)
		return ;
	node_if_data(root->left, act, assert, data);
	if (assert(root))
		act(root->content, data);
	node_if_data(root->right, act, assert, data);
}
