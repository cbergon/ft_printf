/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_clean.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:21:10 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:21:12 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_clean(t_btree *root, void (*clr)())
{
	if (root == NULL)
		return ;
	btree_clean(root->left, clr);
	btree_clean(root->right, clr);
	clr(root->content);
	ft_memdel((void **)(&root));
}
