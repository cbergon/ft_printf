/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btree_insert.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 11:21:36 by clbergon          #+#    #+#             */
/*   Updated: 2018/11/06 11:21:38 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

static t_btree	*insert(t_btree *root, void *content, int (*cmp)())
{
	int						left;

	left = ((*cmp)(content, root->content)) < 0;
	if (left != 0)
	{
		if (root->left == NULL)
			return (root->left = btree_create(content));
		else
			return (insert(root->left, content, cmp));
	}
	else
	{
		if (root->right == NULL)
			return (root->right = btree_create(content));
		else
			return (insert(root->right, content, cmp));
	}
}

t_btree			*btree_insert(t_btree **root, void *content, int (*cmp)())
{
	if (root == NULL)
		return (NULL);
	if (*root == NULL)
		return (*root = btree_create(content));
	else
		return (insert(*root, content, cmp));
}
