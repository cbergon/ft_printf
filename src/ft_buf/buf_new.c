/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:31:45 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:31:48 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_buf.h"

t_buf		*buf_new(void)
{
	return (buf_new_with_size(DEFAULT_CAPACITY));
}

t_buf		*buf_new_with_size(size_t size)
{
	t_buf	*ret;

	if ((ret = (t_buf *)ft_memalloc(sizeof(t_buf))) == NULL)
		return (NULL);
	if ((ret->content = (char *)ft_memalloc(sizeof(size))) == NULL)
	{
		ft_memdel((void *)&ret);
		return (NULL);
	}
	ret->len = 0;
	ret->capacity = size;
	return (ret);
}
