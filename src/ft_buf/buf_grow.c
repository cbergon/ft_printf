/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:31:45 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:31:48 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_buf.h"

int		buf_grow(t_buf *buf)
{
	char	*tmp;

	if (!(tmp = ft_memalloc(buf->capacity * 2)))
		return (0);
	ft_memcpy(tmp, buf->content, buf->len);
	free(buf->content);
	buf->content = tmp;
	buf->capacity *= 2;
	return (1);
}

int		buf_grow_with_size(t_buf *buf, size_t size)
{
	size_t	new_size;
	char	*tmp;

	new_size = (size_t)ft_next_bit((int)buf->len + (int)size);
	if (!(tmp = ft_memalloc(new_size)))
		return (0);
	ft_memcpy(tmp, buf->content, buf->len);
	free(buf->content);
	buf->content = tmp;
	buf->capacity = new_size;
	return (1);
}
