/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:31:45 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:31:48 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_buf.h"

int		buf_push_one(t_buf *buf, const char c)
{
	return (buf_push(buf, &c, 1));
}

int		buf_push_d(t_buf *buf, long long d, size_t n)
{
	int			mod;
	char		c;
	long long	nb;

	if (n == 0)
		return (1);
	mod = (int)(d % 10);
	nb = d / 10;
	c = (mod > 0 ? mod : -mod) + '0';
	if (!buf_push_d(buf, nb, n - 1))
		return (0);
	if (!buf_push_one(buf, c))
		return (0);
	return (1);
}

int		buf_push_base(t_buf *buf, unsigned long long ull, size_t n, char *base)
{
	unsigned int		mod;
	unsigned long long	nb;
	size_t				len;

	if (n == 0)
		return (1);
	len = ft_strlen(base);
	mod = (unsigned int)(ull % len);
	nb = ull / len;
	if (!buf_push_base(buf, nb, n - 1, base))
		return (0);
	if (!buf_push_one(buf, base[mod]))
		return (0);
	return (1);
}

int		buf_push_n(t_buf *buf, const char c, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n)
	{
		if (!buf_push_one(buf, c))
			return (0);
		i++;
	}
	return (1);
}

int		buf_push(t_buf *buf, const char *s, const size_t len)
{
	if (buf->len + len > buf->capacity)
	{
		if (!buf_grow_with_size(buf, len))
			return (0);
	}
	ft_memcpy(buf->content + buf->len, s, len);
	buf->len += len;
	return (1);
}
