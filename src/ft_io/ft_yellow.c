/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_yellow.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <clbergon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 18:49:06 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/30 14:12:56 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_io.h"

void	ft_yellow(char *s)
{
	ft_putstr("\033[33m");
	ft_putstr(s);
	ft_putstr("\033[0m");
}
