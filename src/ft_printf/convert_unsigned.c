/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_unsigned.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:59:54 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:59:56 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static t_ull	get_va_arg(va_list ap, int flag)
{
	if (flag == HH)
		return ((t_ull)(unsigned char)va_arg(ap, unsigned int));
	else if (flag == H)
		return ((t_ull)(unsigned short)va_arg(ap, unsigned int));
	else if (flag == LLOW)
		return ((t_ull)(unsigned long)va_arg(ap, unsigned long));
	else if (flag == LL)
		return ((t_ull)va_arg(ap, t_ull));
	else
		return ((t_ull)va_arg(ap, unsigned int));
}

static int		apply(t_buf *buf, va_list ap, t_conv *conv, t_iull_handler *iu)
{
	int			len;
	t_ull		ull;

	if (!iu->handle_format(conv))
		return (FAILURE);
	ull = get_va_arg(ap, conv->length);
	len = iu->handle_len(ull, conv);
	conv->width = (conv->width > 0 && conv->width > len) ? conv->width : len;
	if (!iu->push_to_buf(buf, conv, ull, len))
		return (FAILURE);
	return (SUCCESS);
}

int				unsigned_conv(t_buf *buf, va_list ap, t_conv *conv)
{
	t_iull_handler	iull;

	if (CONVERSION[conv->type] == 'u')
		iull = assign_unsigned();
	else if (CONVERSION[conv->type] == 'o')
		iull = assign_octal();
	else if (CONVERSION[conv->type] == 'x' || CONVERSION[conv->type] == 'X')
		iull = assign_hexa();
	else if (CONVERSION[conv->type] == 'p')
		iull = assign_pointer();
	else
		return (FAILURE);
	apply(buf, ap, conv, &iull);
	return (SUCCESS);
}
