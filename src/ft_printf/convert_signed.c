/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_signed.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 18:02:55 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 18:02:56 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int			parse_format(t_conv *conv)
{
	if ((conv->flag & FLAG_ZERO) != 0 && conv->prec != -1)
		conv->flag &= ~FLAG_ZERO;
	if ((conv->flag & FLAG_PLUS) != 0 && (conv->flag & FLAG_SPACE) != 0)
		conv->flag &= ~FLAG_SPACE;
	if ((conv->flag & FLAG_MINUS) != 0 && (conv->flag & FLAG_ZERO) != 0)
		conv->flag &= ~FLAG_ZERO;
	return (SUCCESS);
}

static long long	get_va_arg(va_list ap, int flag)
{
	if (flag == HH)
		return ((long long)(char)va_arg(ap, int));
	else if (flag == H)
		return ((long long)(short)va_arg(ap, int));
	else if (flag == LLOW)
		return ((long long)(long)va_arg(ap, long));
	else if (flag == LL)
		return ((long long)va_arg(ap, long long));
	else
		return ((long long)va_arg(ap, int));
}

static int			handle_len(long long d, t_conv *conv)
{
	int			nbrlen;
	int			len;

	nbrlen = ft_ll_nbrlen(d);
	len = ((conv->flag & FLAG_ZERO) && conv->width > nbrlen)
			? conv->width : nbrlen;
	if (conv->prec > len)
		len = conv->prec;
	else
		len = (conv->prec == 0 && d == 0) ? 0 : len;
	if ((d < 0 && conv->width <= nbrlen)
		|| ((conv->flag & (FLAG_PLUS | FLAG_SPACE)
		|| (d < 0))
		&& (conv->flag & FLAG_ZERO) == 0))
		len += 1;
	return (len);
}

static int			d_push(t_buf *buf, t_conv *conv, long long d, int len)
{
	char		flag;
	int			nlen;

	if (!pad_before(buf, conv, len))
		return (FAILURE);
	flag = d < 0 ? '-' : '\0';
	flag = d >= 0 && (conv->flag & FLAG_PLUS) != 0 ? '+' : flag;
	flag = d >= 0 && (conv->flag & FLAG_SPACE) != 0 ? ' ' : flag;
	if (flag != '\0' && !buf_push_one(buf, flag))
		return (FAILURE);
	nlen = (len - ((flag != '\0') && conv->width > (int)ft_ll_nbrlen(d)));
	if (conv->prec == 0 && d == 0)
		nlen = 0;
	if (!buf_push_d(buf, d, nlen))
		return (FAILURE);
	if (!pad_after(buf, conv, len))
		return (FAILURE);
	return (SUCCESS);
}

int					d_conv(t_buf *buf, va_list ap, t_conv *conv)
{
	int			len;
	long long	d;

	if (!parse_format(conv))
		return (FAILURE);
	d = get_va_arg(ap, conv->length);
	len = handle_len(d, conv);
	conv->width = (conv->width > 0 && conv->width > len) ? conv->width : len;
	if (!d_push(buf, conv, d, len))
		return (FAILURE);
	return (SUCCESS);
}
