/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_strings.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:56:25 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:56:27 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static inline int	str_push(t_buf *buf, t_conv *conv, char *s, int len)
{
	if (!pad_before(buf, conv, len))
		return (FAILURE);
	if (!buf_push(buf, s, len))
		return (FAILURE);
	if (!pad_after(buf, conv, len))
		return (FAILURE);
	return (SUCCESS);
}

int					pct_conv(t_buf *buf, va_list ap, t_conv *conv)
{
	(void)ap;
	pad_before(buf, conv, 1);
	if (!buf_push_one(buf, '%'))
		return (FAILURE);
	pad_after(buf, conv, 1);
	return (SUCCESS);
}

int					char_conv(t_buf *buf, va_list ap, t_conv *conv)
{
	char	c;

	c = (char)va_arg(ap, int);
	pad_before(buf, conv, 1);
	if (!buf_push_one(buf, c))
		return (FAILURE);
	pad_after(buf, conv, 1);
	return (SUCCESS);
}

int					str_conv(t_buf *buf, va_list ap, t_conv *conv)
{
	char	*str;
	int		len;

	if ((str = va_arg(ap, char *)) == NULL)
		str = "(null)";
	len = ft_strlen(str);
	if (conv->prec >= 0 && len > conv->prec)
		len = conv->prec;
	conv->width = (conv->width > 0 && conv->width > len) ? conv->width : len;
	if (!str_push(buf, conv, str, len))
		return (FAILURE);
	return (SUCCESS);
}
