/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 18:33:35 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 18:33:37 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		get_flag(const char *format, t_conv *conv, int *index)
{
	int		i;
	int		check;

	i = 0;
	check = FAILURE;
	while ((i = ft_strindex(FLAG, format[*index])) != -1)
	{
		conv->flag |= 1 << i;
		*index += 1;
		check = SUCCESS;
	}
	return (check);
}

int		get_width(const char *format, t_conv *conv, int *index)
{
	int		i;

	i = 0;
	if (!ft_isdigit(format[*index]))
		return (FAILURE);
	conv->width = ft_atoi(format + *index);
	*index += ft_nbrlen(conv->width);
	return (SUCCESS);
}

int		get_prec(const char *format, t_conv *conv, int *index)
{
	int		i;

	i = 0;
	if (format[*index] != '.')
		return (FAILURE);
	*index += 1;
	while (format[*index] == '0')
		(*index)++;
	if (!ft_isdigit(format[*index]))
	{
		conv->prec = 0;
		return (SUCCESS);
	}
	conv->prec = ft_atoi(format + *index);
	*index += ft_nbrlen(conv->prec);
	return (SUCCESS);
}

int		get_length(const char *format, t_conv *conv, int *index)
{
	if (format[*index] == 'L')
		conv->length = LUP;
	else if (format[*index] == 'l')
	{
		if (format[*index + 1] != '\0' && format[*index + 1] == 'l')
			conv->length = LL;
		else
			conv->length = LLOW;
	}
	else if (format[*index] == 'h')
	{
		if (format[*index + 1] != '\0' && format[*index + 1] == 'h')
			conv->length = HH;
		else
			conv->length = H;
	}
	else
		return (FAILURE);
	if (conv->length == LLOW || conv->length == LUP || conv->length == H)
		*index += 1;
	else if (conv->length == LL || conv->length == HH)
		*index += 2;
	return (SUCCESS);
}

int		get_type(const char *format, t_conv *conv, int *index)
{
	conv->type = ft_strindex(CONVERSION, format[*index]);
	if (conv->type != ERR || ft_islower(format[*index]))
		*index += 1;
	return (SUCCESS);
}
