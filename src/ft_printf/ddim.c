/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ddim.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 18:07:04 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 18:07:05 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ddim_init(t_ddim *ddim, long double *d, int plus_flag,
							int space_flag)
{
	*ddim = (t_ddim){ 0, 0, 0, 0, -1, -1, -1, 0, };
	if (*d < 0)
	{
		ddim->sign = '-';
		*d = -(*d);
		*d = (*d < 0) ? -(*d - 1) : *d;
	}
	else if (plus_flag)
		ddim->sign = '+';
	else if (space_flag)
		ddim->sign = ' ';
	else
		ddim->sign = '\0';
	ddim->elem = ft_ll_nbrlen((long long)*d);
}

static void		ddim_set_total(t_ddim *ddim, int prec)
{
	ddim->prec = prec;
	ddim->total = ddim->elem;
	ddim->total += ddim->sign != '\0';
	if (prec == -1)
		ddim->total += (6 + 1);
	else if (prec > 0)
		ddim->total += (prec + 1);
}

static void		ddim_set_padding(t_ddim *ddim, int width, int zeropad,
									int minus)
{
	int			pad_len;

	if (ddim->total >= width)
		return ;
	pad_len = width - ddim->total;
	if (minus)
		ddim->right_pad = pad_len;
	else if (zeropad)
		ddim->zero_pad = pad_len;
	else
		ddim->left_pad = pad_len;
}

t_ddim			*get_dims(long double *d, t_conv *conv)
{
	static		t_ddim dims;

	ddim_init(&dims, d, conv->flag & FLAG_PLUS, conv->flag & FLAG_SPACE);
	ddim_set_total(&dims, conv->prec);
	ddim_set_padding(&dims, conv->width, conv->flag & FLAG_ZERO,
						conv->flag & FLAG_MINUS);
	return (&dims);
}
