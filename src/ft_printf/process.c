/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conversion.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:31:45 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:31:48 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			init_struct(t_conv *conv, const char *format, int *index)
{
	ft_bzero(conv, sizeof(t_conv));
	conv->prec = -1;
	while (format[*index] != '\0')
	{
		if (get_length(format, conv, index))
			continue;
		else if (get_prec(format, conv, index))
			continue;
		else if (get_flag(format, conv, index))
			continue;
		else if (get_width(format, conv, index))
			continue;
		else if (ft_isalpha(format[*index])
					|| format[*index] == '%'
					|| format[*index] == '\0')
			break ;
		(*index)++;
	}
	if (!get_type(format, conv, index))
		return (FAILURE);
	return (SUCCESS);
}

t_handler	assign(int id)
{
	const t_handler		handlers[] = {
		char_conv,
		str_conv,
		unsigned_conv,
		d_conv,
		d_conv,
		unsigned_conv,
		unsigned_conv,
		unsigned_conv,
		unsigned_conv,
		double_conv,
		pct_conv,
		NULL
	};

	if (id == ERR)
		return (err_handler);
	if (id < 0 || id >= FUNCS)
		return (NULL);
	return (handlers[id]);
}

int			convert(const char *format, int *index, t_buf *buf, va_list ap)
{
	t_conv		conv;
	t_handler	handle;

	if (!(init_struct(&conv, format, index)))
		return (FAILURE);
	if (!(handle = assign(conv.type)))
		return (FAILURE);
	return (handle(buf, ap, &conv));
}

int			get_data(const char *format, int *index, t_buf *buf)
{
	while (format[*index] != '\0' && format[*index] != '%')
	{
		if (!buf_push_one(buf, format[*index]))
			return (FAILURE);
		(*index)++;
	}
	return (SUCCESS);
}

int			process(const char *format, t_buf *buf, va_list ap)
{
	int		index;

	index = 0;
	while (format[index] != '\0')
	{
		if (format[index] == '%')
		{
			index++;
			if (!convert(format, &index, buf, ap))
				continue;
		}
		else if (!(get_data(format, &index, buf)))
		{
			buf_free(&buf);
			return (FAILURE);
		}
	}
	return (SUCCESS);
}
