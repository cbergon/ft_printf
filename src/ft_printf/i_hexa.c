/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_hexa.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:49:13 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:49:15 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		handle_format(t_conv *conv)
{
	if ((conv->flag & FLAG_ZERO) != 0 && conv->prec != -1)
		conv->flag &= ~FLAG_ZERO;
	if ((conv->flag & FLAG_MINUS) != 0 && (conv->flag & FLAG_ZERO) != 0)
		conv->flag &= ~FLAG_ZERO;
	return (SUCCESS);
}

static int		handle_len(t_ull ull, t_conv *conv)
{
	int		nbrlen;
	int		len;

	if (ull == 0)
		conv->flag &= ~FLAG_HASH;
	nbrlen = ft_base_nbrlen(ull, 16);
	len = ((conv->flag & FLAG_ZERO) && conv->width > nbrlen)
			? conv->width : nbrlen;
	if (conv->prec > len)
		len = conv->prec;
	else
		len = (conv->prec == 0 && ull == 0) ? 0 : len;
	return (len);
}

static int		calc_len(t_ull ull, t_conv *conv)
{
	int			width;
	int			prec;
	int			len;

	len = ft_base_nbrlen(ull, 16);
	width = (conv->flag & FLAG_ZERO && conv->width > 0) ? conv->width : 0;
	width = (conv->flag & FLAG_HASH) ? (width - 2) : width;
	prec = (conv->prec > 0) ? conv->prec : 0;
	len = len > width ? len : width;
	len = len > prec ? len : prec;
	len = (conv->prec == 0 && ull == 0) ? 0 : len;
	return (len);
}

static int		push_to_buf(t_buf *buf, t_conv *conv, t_ull ull, int len)
{
	char	flag;
	char	type;

	type = CONVERSION[conv->type];
	if ((conv->flag & (FLAG_HASH | FLAG_ZERO)) == FLAG_HASH)
		conv->width -= 2;
	if (!pad_before(buf, conv, len))
		return (FAILURE);
	flag = ((conv->flag & FLAG_HASH) != 0) ? type : '\0';
	if (flag == type && ull != 0
		&& (!buf_push_one(buf, '0') || !buf_push_one(buf, type)))
		return (FAILURE);
	if (!buf_push_base(buf, ull, calc_len(ull, conv),
						type == 'x' ? BASE_LOWER : BASE_UPPER))
		return (FAILURE);
	if (!pad_after(buf, conv, len))
		return (FAILURE);
	return (SUCCESS);
}

t_iull_handler	assign_hexa(void)
{
	return ((t_iull_handler){handle_format, handle_len, push_to_buf});
}
