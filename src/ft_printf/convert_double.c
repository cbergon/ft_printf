/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   convert_double.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 18:07:21 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 18:07:24 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int			handle_flo_pre(char *buf, size_t *len, int prec,
								long double f)
{
	size_t			pre;
	int				tmp_i;

	buf[(*len)] = '.';
	(*len)++;
	pre = (prec != -1) ? prec : 6;
	f += rounder(pre + 1);
	while (pre != 0)
	{
		if (f == 0)
		{
			buf[(*len)++] = '0';
			pre--;
			continue ;
		}
		f *= 10;
		tmp_i = (int)(f);
		f -= tmp_i;
		pre--;
		buf[(*len)] = '0' + tmp_i;
		(*len)++;
	}
	return (1);
}

static long double	get_va_arg(va_list ap, t_conv *conv)
{
	if (conv->length & LUP)
		return (va_arg(ap, long double));
	return ((long double)va_arg(ap, double));
}

static int			to_buf(char doubbuf[DOUB_BUFSIZ], t_ddim *ddim, t_buf *buf,
							size_t len)
{
	if (!ddim_to_buf(buf, ' ', ddim->left_pad)
		|| !ddim_to_buf_sign(ddim, buf)
		|| !ddim_to_buf(buf, '0', ddim->zero_pad)
		|| !buf_push(buf, doubbuf, len)
		|| !ddim_to_buf(buf, ' ', ddim->right_pad))
		return (FAILURE);
	return (SUCCESS);
}

int					double_conv(t_buf *buf, va_list ap, t_conv *conv)
{
	long double	d;
	int			prec;
	char		doubbuf[DOUB_BUFSIZ];
	size_t		len;
	t_ddim		*ddim;

	ft_bzero(doubbuf, DOUB_BUFSIZ);
	d = get_va_arg(ap, conv);
	prec = (conv->prec != -1) ? conv->prec : 6;
	ddim = get_dims(&d, conv);
	ddim->real = (t_ull)(d + rounder(prec + 1));
	fpf_ultobuf(ddim->real, 10, BASE_LOWER, doubbuf);
	len = get_ull_len(ddim->real, 10);
	if (ddim->prec)
		handle_flo_pre(doubbuf, &len, ddim->prec, d - ddim->real);
	else if (!ddim->prec && conv->flag & FLAG_HASH)
	{
		doubbuf[len] = '.';
		len++;
	}
	if (!to_buf(doubbuf, ddim, buf, len))
		return (FAILURE);
	return (SUCCESS);
}
