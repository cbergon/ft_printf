/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ddim_to_buf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 18:07:11 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 18:07:12 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ddim_to_buf(t_buf *buf, char c, int n)
{
	if (n == -1)
		return (SUCCESS);
	return (buf_push_n(buf, c, n));
}

int			ddim_to_buf_sign(t_ddim *ddim, t_buf *buf)
{
	if (ddim->sign == '\0')
		return (SUCCESS);
	return (buf_push_one(buf, ddim->sign));
}
