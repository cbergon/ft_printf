/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:38:56 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:38:57 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		pad_before(t_buf *buf, t_conv *conv, int len)
{
	if (conv->width > len && (conv->flag & (1 << 1)) == 0)
	{
		if (!buf_push_n(buf, ' ', conv->width - len))
			return (FAILURE);
	}
	return (SUCCESS);
}

int		pad_after(t_buf *buf, t_conv *conv, int len)
{
	if (conv->width > len && (conv->flag & (1 << 1)) != 0)
	{
		if (!buf_push_n(buf, ' ', conv->width - len))
			return (FAILURE);
	}
	return (SUCCESS);
}

int		err_handler(t_buf *buf, va_list ap, t_conv *conv)
{
	char	c;

	(void)ap;
	c = (conv->flag & FLAG_ZERO) ? '0' : ' ';
	if ((conv->width - 1) > 0)
		buf_push_n(buf, c, conv->width - 1);
	return (SUCCESS);
}
