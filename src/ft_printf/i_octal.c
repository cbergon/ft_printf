/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_octal.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 17:46:57 by clbergon          #+#    #+#             */
/*   Updated: 2019/12/12 17:46:59 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		handle_format(t_conv *conv)
{
	if (conv->prec == 0 && (conv->flag & FLAG_HASH) != 0)
		conv->prec = -1;
	if ((conv->flag & FLAG_ZERO) != 0 && conv->prec != -1)
		conv->flag &= ~FLAG_ZERO;
	if ((conv->flag & FLAG_MINUS) != 0 && (conv->flag & FLAG_ZERO) != 0)
		conv->flag &= ~FLAG_ZERO;
	return (SUCCESS);
}

static int		handle_len(t_ull ull, t_conv *conv)
{
	int		nbrlen;
	int		len;

	if (ull == 0)
		conv->flag &= ~FLAG_HASH;
	nbrlen = ft_base_nbrlen(ull, 8) + ((conv->flag & FLAG_HASH) != 0);
	len = ((conv->flag & FLAG_ZERO) && conv->width > nbrlen)
			? conv->width : nbrlen;
	if (conv->prec > len)
		len = conv->prec;
	else
		len = (conv->prec == 0) ? 0 : len;
	return (len);
}

static int		push_to_buf(t_buf *buf, t_conv *conv, t_ull ull, int len)
{
	char	flag;

	if (!pad_before(buf, conv, len))
		return (FAILURE);
	flag = ((conv->flag & FLAG_HASH) != 0) ? '0' : '\0';
	if (flag != '\0' && ull != 0 && !buf_push_one(buf, flag))
		return (FAILURE);
	if (!buf_push_base(buf, ull, (len - ((flag == '\0') ? 0 : 1)), BASE_OCTAL))
		return (FAILURE);
	if (!pad_after(buf, conv, len))
		return (FAILURE);
	return (SUCCESS);
}

t_iull_handler	assign_octal(void)
{
	return ((t_iull_handler){handle_format, handle_len, push_to_buf});
}
