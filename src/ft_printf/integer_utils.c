/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   integer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbastion <vbastion@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/29 13:56:23 by vbastion          #+#    #+#             */
/*   Updated: 2017/08/09 09:55:47 by vbastion         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	get_ull_len(uintmax_t ull, size_t base)
{
	size_t		len;

	len = 0;
	if (ull == 0)
		return (1);
	while (ull != 0)
	{
		len++;
		ull /= base;
	}
	return (len);
}

double	rounder(int p)
{
	double		d;

	d = 5.;
	while (p > 0)
	{
		d /= 10;
		p--;
	}
	return (d);
}

void	fpf_ultobuf(t_ull ull, size_t base, char *charset, char *buf)
{
	size_t		pos;
	t_uc		tmp;
	size_t		len;

	len = get_ull_len(ull, base);
	pos = 0;
	if (ull == 0)
	{
		ft_strncpy(buf, "0", 1);
		return ;
	}
	while (ull != 0)
	{
		tmp = ull % base;
		buf[len - pos - 1] = charset[tmp];
		pos++;
		ull /= base;
	}
}
