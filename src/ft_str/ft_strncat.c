/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/17 16:13:48 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/17 16:54:45 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	int		i;
	int		size;

	i = 0;
	size = 0;
	while (s1[size] != '\0')
		size++;
	while (s2[i] != '\0' && n > 0)
	{
		s1[size] = s2[i];
		i++;
		size++;
		n--;
	}
	s1[size] = '\0';
	return (s1);
}
