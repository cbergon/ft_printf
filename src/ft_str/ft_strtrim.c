/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 12:10:34 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/23 17:13:32 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

static int		get_len(char const *s)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && (s[i] != '\0'))
	{
		j++;
		i++;
	}
	if (s[i] == '\0')
		return (0);
	i = ft_strlen(s);
	while (s[i - 1] == ' ' || s[i - 1] == '\n' || s[i - 1] == '\t')
	{
		j++;
		i--;
	}
	return (ft_strlen(s) - j);
}

static int		skip_spaces(char const *s)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while ((s[i] != '\0') && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t'))
	{
		j++;
		i++;
	}
	return (j);
}

char			*ft_strtrim(char const *s)
{
	int		i;
	int		j;
	int		size;
	char	*str;

	i = 0;
	if (!s)
		return (ft_strnew(1));
	size = get_len(s);
	if (!(str = ft_strnew(size)))
		return (NULL);
	j = skip_spaces(s);
	if (s[j] == '\0')
		return (ft_strnew(0));
	i = 0;
	while (i < size)
	{
		str[i] = s[j];
		i++;
		j++;
	}
	str[i] = '\0';
	return (str);
}
