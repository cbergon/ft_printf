/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 18:00:31 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/28 18:05:11 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

char			*ft_itoa(int n)
{
	char			*res;
	unsigned int	abs;
	int				size;
	int				sign;

	sign = 0;
	if (n < 0)
		sign = 1;
	abs = (sign) ? -n : n;
	size = (n == 0) ? 1 : ft_nbrlen(abs);
	if (!(res = ft_strnew(size + sign)))
		return (NULL);
	if (sign)
		res[0] = '-';
	while (size)
	{
		res[size - ((sign) ? 0 : 1)] = (abs % 10) + '0';
		abs /= 10;
		size--;
	}
	return (res);
}
