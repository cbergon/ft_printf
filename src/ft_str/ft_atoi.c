/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 21:41:46 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/22 21:44:30 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long	ft_atoi(const char *s)
{
	long	i;
	long	negatif;
	long	nb;

	i = 0;
	negatif = 0;
	nb = 0;
	while (s[i] == ' ' || s[i] == '\r' || s[i] == '\f' || s[i] == '\n'\
			|| s[i] == '\t' || s[i] == '\v')
		i++;
	if (s[i] == 45)
		negatif = 1;
	if (s[i] == 45 || s[i] == 43)
		i++;
	while (s[i] >= 48 && s[i] <= 57)
	{
		nb *= 10;
		nb += (s[i] - 48);
		i++;
	}
	if (negatif == 1)
		return (-nb);
	else
		return (nb);
}
