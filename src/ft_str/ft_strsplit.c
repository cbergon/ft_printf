/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clbergon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 11:14:07 by clbergon          #+#    #+#             */
/*   Updated: 2017/11/23 17:55:59 by clbergon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_str.h"

static int		size_word(char const *s, char c)
{
	int i;

	i = 0;
	while (s[i] && s[i] != c)
	{
		i++;
	}
	return (i);
}

static int		create_word(char const *s, char *word, char c)
{
	int		i;
	int		j;

	i = 0;
	j = size_word(s, c);
	while (i < j)
	{
		word[i] = s[i];
		i++;
	}
	word[i] = '\0';
	return (i);
}

static int		count_words(char const *s, char c)
{
	int		i;
	int		word_count;

	i = 0;
	word_count = 0;
	while (s[i])
	{
		while (s[i] == c)
			i++;
		if (s[i])
			word_count++;
		while (s[i] && s[i] != c)
			i++;
	}
	return (word_count);
}

char			**ft_strsplit(char const *s, char c)
{
	int		i;
	int		j;
	char	**tab;

	i = 0;
	j = 0;
	if (!s)
		return (NULL);
	if (!(tab = ft_memalloc(sizeof(char*) * (count_words(s, c) + 1))))
		return (NULL);
	while (s[i] != '\0')
	{
		while (s[i] == c)
			i++;
		if (s[i] && s[i] != c)
		{
			if (!(tab[j] = ft_strnew(size_word(s + i, c) + 1)))
				return (NULL);
			i = i + create_word((s + i), tab[j], c);
			j++;
		}
	}
	return (tab);
}
